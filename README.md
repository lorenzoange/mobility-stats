# Usage

Install Docker with:
```
curl -sSL https://get.docker.com/ | sh
```

## cassandra
We use Cassandra official Datastax container from Docker repo.

Port usage:

- 7000: intra-node communication
- 7001: TLS intra-node communication
- 7199: JMX
- 9042: CQL
- 9160: thrift service

Launch with:
```
sudo docker run -p 9042:9042 -p 7000:7000 -d -e CASSANDRA_BROADCAST_ADDRESS=<THIS_NODE_IP> cassandra:2.1
```

Log into cassandra container cqlsh console:
```
sudo docker exec -it <CONTAINER_NAME> cqlsh
```

Copy and paste `cassandra_init.cql` file content in cqlsh console to init keyspaces and columnfamilies.

Add a node to the cassandra cluster:
```
sudo docker run -d -e CASSANDRA_BROADCAST_ADDRESS=<THIS_NODE_IP> -p 9042:9042 -p 7000:7000 -e CASSANDRA_SEEDS=<FIRST_NODE_IP> cassandra:2.1
```

Check cassandra cluster nodes status:
```
sudo docker exec -it <CONTAINER_NAME> bash
nodetool status
```

## spark-master

Build with:
```
cd spark-master
sudo docker build -t sm .
```

Port usage:

- 8080: Web UI, show cluster workers
- 7077: Workers communication
- 80: Web service for sql query execution

Launch with:
```
sudo docker run -p 8080:8080 -p 7077:7077 -p 80:80 sm &
```

NOTE: this container must be able to communicate with cassandra

## spark-worker

Build with:
```
cp spark-master/spark-2.0.1-bin-hadoop2.7.tgz spark-worker/spark-2.0.1-bin-hadoop2.7.tgz
cd spark-worker
sudo docker build -t sw .
```

Port usage:

- 8081: Web UI, show worker status

Launch with:
```
sudo docker run -p 8081:8081 -e "MASTER_SPARK_URL=spark://<SPARK_MASTER_IP>:7077" sw &
```

## rtbusfaker-cassandra-datasource

Build with:
```
cd rtbusfaker-cassandra-datasource
sudo docker build -t rcd .
```

Launch with:
```
sudo docker run -e CASSANDRA_IP=<CASSANDRA_NODE_IP> rcd &
```

## aggregate-data-visualization

Build with:
```
cd aggregate-data-visualization
sudo docker build -t adv .
```

Launch with:
```
sudo docker run -p 80:80 -e CASSANDRA_NODE_IP=<CASSANDRA_NODE_IP> -e SPARK_MASTER_IP=<SPARK_MASTER_IP> adv &
```

## Lambda architecture for stops average delay calculation

First log into spark master container bash console:
```
sudo docker exec -it <CONTAINER_NAME> bash
```

Calculate and save to Cassandra the Batch View:
```
cd /usr/local
./spark-2.0.1-bin-hadoop2.7/bin/spark-submit --packages com.datastax.spark:spark-cassandra-connector_2.11:2.0.0-M3 stop_avg_d_batch_view.py "<CASSANDRA_NODE_IP>"
```

Calculate and save to Cassandra the Real Time View:
```
cd /usr/local
./spark-2.0.1-bin-hadoop2.7/bin/spark-submit --packages com.datastax.spark:spark-cassandra-connector_2.11:2.0.0-M3 stop_avg_d_rt_view.py "<CASSANDRA_NODE_IP>"
```

Show the Serving Layer:
```
cd /usr/local
./spark-2.0.1-bin-hadoop2.7/bin/spark-submit --packages com.datastax.spark:spark-cassandra-connector_2.11:2.0.0-M3 stop_avg_d_serving_layer.py "<CASSANDRA_NODE_IP>"
```



