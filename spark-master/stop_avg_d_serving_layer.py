import sys
from pyspark.sql import SparkSession

cassandra_node_ip = sys.argv[1]
sqlContext = SparkSession.builder.appName("Stop AVG Delay Serving Layer").config("spark.cassandra.connection.host", cassandra_node_ip).getOrCreate()

keyspace = "aggregate_data"
table = "stops_avg_delay_batch"
temp_view_query = "CREATE TEMPORARY VIEW %(keyspace)s%(table)s USING org.apache.spark.sql.cassandra OPTIONS (table \"%(table)s\", keyspace \"%(keyspace)s\")"%locals()
sqlContext.sql(temp_view_query)

keyspace = "aggregate_data"
table = "stops_avg_delay_speed"
temp_view_query = "CREATE TEMPORARY VIEW %(keyspace)s%(table)s USING org.apache.spark.sql.cassandra OPTIONS (table \"%(table)s\", keyspace \"%(keyspace)s\")"%locals()
sqlContext.sql(temp_view_query)

query = ("SELECT "
         "b.avg_delay_mins AS avg_d_b, "
         "rt.avg_delay_mins AS avg_d_rt, "
         "b.records_n AS records_n_b, "
         "rt.records_n AS records_n_rt, "
         "b.stop_id as stop_id, "
         "ROUND((b.avg_delay_mins*b.records_n + rt.avg_delay_mins*rt.records_n)/(b.records_n + rt.records_n), 1) as avg_delay_mins "
         "FROM aggregate_datastops_avg_delay_batch b "
         "JOIN aggregate_datastops_avg_delay_speed rt "
         "ON b.stop_id = rt.stop_id")

df = sqlContext.sql(query)
df.show()