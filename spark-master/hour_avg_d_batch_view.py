import sys
from pyspark.sql import SparkSession

cassandra_node_ip = sys.argv[1]
sqlContext = SparkSession.builder.appName("Hour AVG Delay Batch View Generator").config("spark.cassandra.connection.host", cassandra_node_ip).getOrCreate()

keyspace = "aggregate_data"
table = "vehicle_last_stops"
temp_view_query = "CREATE TEMPORARY VIEW %(keyspace)s%(table)s USING org.apache.spark.sql.cassandra OPTIONS (table \"%(table)s\", keyspace \"%(keyspace)s\")"%locals()
sqlContext.sql(temp_view_query)

query = ("SELECT "
         "'bo' AS partition, "
         "HOUR(FROM_UNIXTIME(expected_timestamp/1000)) as hour, "
         "ROUND(AVG(timestamp - expected_timestamp)/60000, 1) AS avg_delay_mins, "
         "COUNT(*) AS records_n, "
         "unix_timestamp()*1000 AS timestamp "
         "FROM aggregate_datavehicle_last_stops "
         "GROUP BY HOUR(FROM_UNIXTIME(expected_timestamp/1000))")
df = sqlContext.sql(query)
df.show()

df.write\
  .format("org.apache.spark.sql.cassandra")\
  .mode('append')\
  .options(table="hours_avg_delay_batch", keyspace="aggregate_data")\
  .save()