import sys
from pyspark.sql import SparkSession
from operator import methodcaller

cassandra_node_ip = sys.argv[1]
# "keyspace1.table1,kespace2.table2,..."
keyspaces_tables = list(map(methodcaller("split", "."), sys.argv[2].split(",")))
spark_sql_query = sys.argv[3]

sqlContext = SparkSession.builder.appName("Python Spark SQL query performer").config("spark.cassandra.connection.host", cassandra_node_ip).getOrCreate()

for keyspace_table in keyspaces_tables:
  keyspace = keyspace_table[0]
  table = keyspace_table[1]
  temp_view_query = "CREATE TEMPORARY VIEW %(keyspace)s%(table)s USING org.apache.spark.sql.cassandra OPTIONS (table \"%(table)s\", keyspace \"%(keyspace)s\")"%locals()
  sqlContext.sql(temp_view_query)

df = sqlContext.sql(spark_sql_query)

df.show()