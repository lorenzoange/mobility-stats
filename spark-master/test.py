from pyspark.streaming import StreamingContext
from __future__ import print_function
from pyspark.sql import SparkSession, Row
from pyspark import SparkConf, SparkContext
import datetime

# TODO: REMOVE ME!!!
sc.stop()

conf = (SparkConf()
         .setAppName("PythonStreamingStopCheck")
         .set("spark.cassandra.connection.host", "146.148.13.66"))
sc = SparkContext(conf = conf)
ssc = StreamingContext(sc, 60)
sql_context = SQLContext(sc)

def process_tokens(time, rdd):
    print("========= %s =========" % str(time))
    sql_context = SQLContext(sc)
    sql_context.sql("CREATE OR REPLACE TEMPORARY VIEW gtfs_bostops USING org.apache.spark.sql.cassandra OPTIONS (table \"stops\", keyspace \"gtfs_bo\")")
    for tokens in rdd.collect():
        trip = tokens[1]
        route = tokens[2]
        latitude = tokens[3]
        longitude = tokens[4]
        timestamp = tokens[5]
        # Nearest stop, distance calculated with haversine algorithm
        query = """SELECT stop_id AS id, stop_name AS name, stop_lat AS lat, stop_lon AS lon,
                   111.045* DEGREES(ACOS(COS(RADIANS(latpoint))
                             * COS(RADIANS(CAST(stop_lat AS Float)))
                             * COS(RADIANS(longpoint) - RADIANS(CAST(stop_lon AS Float)))
                             + SIN(RADIANS(latpoint))
                             * SIN(RADIANS(CAST(stop_lat AS Float))))) AS distance_in_km
                   FROM gtfs_bostops
                   JOIN (
                       SELECT %(latitude)s AS latpoint, %(longitude)s AS longpoint
                     ) AS p ON 1=1
                   ORDER BY distance_in_km
                   LIMIT 1""" % locals()
        nearest_stop = sql_context.sql(query).rdd.first()
        if(nearest_stop.distance_in_km < 0.001):
            print("Bus %s just arrived at stop %s" % (route, nearest_stop.name))
            sql_context.sql("CREATE OR REPLACE TEMPORARY VIEW gtfs_bostop_times USING org.apache.spark.sql.cassandra OPTIONS (table \"stop_times\", keyspace \"gtfs_bo\")")
            query = "SELECT arrival_time FROM gtfs_bostop_times WHERE trip_id = \"%s\" AND stop_id = \"%s\"" % (trip, nearest_stop.id)
            result = sql_context.sql(query)
            if(result.count() != 0):
                expected_time = result.rdd.first().arrival_time.split(":")
                expected_datetime = datetime.datetime.now().replace(hour=int(expected_time[0]), minute=int(expected_time[1]), second=int(expected_time[2]))
                expected_timestamp = long(expected_datetime.strftime("%s")) * 1000
                timestamp = long(timestamp)
                print("arrival_time: %s, expected_time: %s, difference: %s" % (timestamp, expected_timestamp, timestamp - expected_timestamp))
                df = sql_context.createDataFrame([Row(id = "%s__%s" % (trip, timestamp), trip_id = trip, route_id = route, stop_id = nearest_stop.id, timestamp = timestamp, expected_timestamp = expected_timestamp)])
                df.write\
                  .format("org.apache.spark.sql.cassandra")\
                  .mode('append')\
                  .options(table="vehicle_last_stops", keyspace="aggregate_data_bo")\
                  .save()
                #sc.parallelize([{ "id": "%s__%s" % (trip, timestamp), "trip_id": trip, "route_id": route, "stop_id": nearest_stop.id, "timestamp": timestamp, "expected_timestamp": expected_timestamp }]).saveToCassandra("aggregate_data_bo", "vehicle_last_stops")


lines = ssc.socketTextStream("146.148.10.60", 9999)

lines.map(lambda line: line.split(","))\
     .foreachRDD(process_tokens)

ssc.start()