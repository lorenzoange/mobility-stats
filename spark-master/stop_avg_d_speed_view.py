import sys
from pyspark.sql import SparkSession

cassandra_node_ip = sys.argv[1]
sqlContext = SparkSession.builder.appName("Stop AVG Delay Real Time View Generator").config("spark.cassandra.connection.host", cassandra_node_ip).getOrCreate()

keyspace = "aggregate_data"
table = "vehicle_last_stops"
temp_view_query = "CREATE TEMPORARY VIEW %(keyspace)s%(table)s USING org.apache.spark.sql.cassandra OPTIONS (table \"%(table)s\", keyspace \"%(keyspace)s\")"%locals()
sqlContext.sql(temp_view_query)

keyspace = "aggregate_data"
table = "stops_avg_delay_batch"
temp_view_query = "CREATE TEMPORARY VIEW %(keyspace)s%(table)s USING org.apache.spark.sql.cassandra OPTIONS (table \"%(table)s\", keyspace \"%(keyspace)s\")"%locals()
sqlContext.sql(temp_view_query)

last_batch_timestamp_query = "SELECT timestamp FROM aggregate_datastops_avg_delay_batch LIMIT 1"

query = ("SELECT "
         "'bo' AS partition, "
         "stop_id, "
         "(%(last_batch_timestamp_query)s) AS batch_timestamp, "
         "ROUND(AVG(timestamp - expected_timestamp)/60000, 1) AS avg_delay_mins, "
         "COUNT(*) AS records_n "
         "FROM aggregate_datavehicle_last_stops "
         "WHERE timestamp > (%(last_batch_timestamp_query)s) "
         "GROUP BY stop_id") % locals()
df = sqlContext.sql(query)
df.show()

df.write\
  .format("org.apache.spark.sql.cassandra")\
  .mode('append')\
  .options(table="stops_avg_delay_speed", keyspace="aggregate_data")\
  .save()