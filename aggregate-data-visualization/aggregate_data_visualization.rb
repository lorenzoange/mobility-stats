require 'sinatra'
require 'sinatra-websocket'
require 'cassandra'
require 'json'
require 'uri'
require 'net/http'


set :port, 80
set :bind, '0.0.0.0'
set :sockets, []

get '/' do
  @cassandra_node_ip = ENV['CASSANDRA_NODE_IP']
  @spark_master_ip = ENV['SPARK_MASTER_IP']
  erb :index
end

post '/spark_sql_query.json' do
  uri = URI.parse("http://#{params['spark_master_ip']}/spark_sql_query_executor.json")

  spark_params = {}
  spark_params["keyspaces_tables"] = params['keyspaces_tables']
  spark_params["spark_sql_query"] = params['spark_sql_query']
  spark_params["cassandra_node_ip"] = params['cassandra_node_ip']

  response = Net::HTTP.post_form(uri, spark_params)
  response.body
end

post '/simple_cql_query.json' do
  erb :simple_cql_query
end

get '/user_query' do
  @cassandra_node_ip = params[:cassandra_node_ip]
  @spark_master_ip = params[:spark_master_ip]
  return "'cassandra_node_ip', 'spark_master_ip' params required" if @cassandra_node_ip.to_s == '' || @spark_master_ip.to_s == ''
  erb :user_query
end

get '/charts' do
  @cassandra_node_ip = params[:cassandra_node_ip]
  @spark_master_ip = params[:spark_master_ip]
  return "'cassandra_node_ip', 'spark_master_ip' params required" if @cassandra_node_ip.to_s == '' || @spark_master_ip.to_s == ''
  erb :charts
end

get '/realtime_map' do
  @cassandra_node_ip = params[:cassandra_node_ip]

  if !request.websocket?
    return "'cassandra_node_ip' param required" if @cassandra_node_ip.to_s == ''
    erb :realtime_map
  else
    request.websocket do |ws|
      ws.onopen do
        @cluster = Cassandra.cluster(hosts: [@cassandra_node_ip])
        @session  = @cluster.connect
        vls_query = "SELECT * FROM aggregate_data.vehicle_last_stops WHERE partition = 'BO' LIMIT 300"
        vpe_query = "SELECT * FROM gtfs_rt.vehicle_positions_entities WHERE partition = 'BO' LIMIT 150"
        loop do
          vls_future = @session.execute_async(vls_query)
          vls_future.on_success do |vls_res|
            vpe_future = @session.execute_async(vpe_query)
            vpe_future.on_success do |vpe_res|
              vpe_res.each do |vpe|
                last_stops = vls_res.select { |vls| vpe['vehicle__trip__trip_id'] == vls['trip_id'] }
                last_stop_delay = 0
                circle_color = 'cyan'
                if !last_stops.empty?
                  last_stop = last_stops.first
                  last_stop_delay = last_stop['timestamp'].to_i - last_stop['expected_timestamp'].to_i
                  circle_color = case
                    when last_stop_delay > 60_000
                      'red'
                    when last_stop_delay < -60_000
                      'yellow'
                    else
                      'green'
                  end
                end
                circle_radius = last_stop_delay.abs/10000 + 50
                ws.send("{ \"trip_id\": \"#{vpe['vehicle__trip__trip_id']}\", \"lat\": #{vpe['vehicle__position__latitude']}, \"lon\": #{vpe['vehicle__position__longitude']}, \"color\": \"#{circle_color}\", \"radius\": #{circle_radius} }")
              end
            end
            vpe_future.join
          end
          vls_future.join
          sleep 0.5
        end
      end

      ws.onclose do
        warn("websocket closed")
        @session.close
        @cluster.close
      end
    end
  end
end
